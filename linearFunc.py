def linearF(x0,y0,x1,y1):
    a = (y1-y0)/(x1-x0)
    b = y0 - a*x0
    return a, b

for i in range(int(raw_input())):
    x0,y0,x1,y1 = raw_input().split()
    ab = list(linearF(int(x0),int(y0),int(x1),int(y1)))
    print("({} {}) ".format(ab[0],ab[1]))